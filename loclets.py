#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  3 15:03:21 2019

"""
import math as m
import numpy as np
import pickle
from scipy.stats import chi2

def g(x,b=2):
    if x < 0:
        return 1
    elif x>=0 and x<=1/b:
        return 1
    elif x>=1/b and x<=1:
        return b*x/(1-b)+b/(b-1)
    else:
        return 0

def zeta(x,k,b=2):
    if k==0:
        return g(x,b)
    else:
        return g(b**(-k)*x)-g(b**(-k+1)*x)
    
def Gf(x, phi, psi, scales):
	G_x = m.pow(phi(x), 2)
	for s in scales:
		G_x += m.pow( psi(s * x), 2 )
	return G_x

def P_I(x, I_min, I_max):
	# indicator function of interval I
    assert I_min < I_max, "I_min should be less than I_max"
    if x < I_min:
        return 0.0
    elif x > I_max:
        return 0.0
    else:
        return 1.0
    
def translate_kernel(kernel, s, a, y):
	# inputs: kernel = psi, phi
	# output: g(y) = psi(s * x)
    
	# if domain(psi) = [0, 2*a]
	# use cdv x = a * (y + 1) to have y in [-1, 1]
	return kernel(s * a * (y+1))

def compute_coeffs(f, N):
	coeffs = []
	for k in range(N):
		s = [ f( m.cos( m.pi * (l+0.5) / float(N) ) ) * m.cos( m.pi * k * (l+0.5) / float(N) ) for l in range(N) ]
		S = sum(s)
		c_k = (2.0 / float(N)) * S
		coeffs.append(c_k)
	return coeffs

def compute_polynomials_matrices_againstFunction(Translated_L, f, N):
	# T_L translation of L to have spectrum in [-1, 1]
	# N Chebyshev truncation parameter
	T_0 = f
	T_1 = np.dot(Translated_L, f)
	polynomials = [ T_0, T_1 ]
	T_k, T_l = T_1, T_0
	for k in range(N-2):
		T = np.multiply( 2.0 , np.dot(Translated_L, T_k)  ) - T_l
		polynomials.append(T)
		T_l = T_k
		T_k = T
	return polynomials
    
def SGWT_coeff(kernel, L, f, s, N, lambda_1, n, O=[], coeffs_diag=[]):
	# inputs: N Chebyshev truncation parameter
	# n size of L
	# output: approximation of kernel(sL)f 
	# approximation of translated function g(y) by Chebyshev T_k(y)
	a = lambda_1 / 2.0
	g = lambda y: translate_kernel(kernel, s, a, y)
	coeffs = compute_coeffs(g, N)
	# computation of T_k(L)f for all necessary k
	aL = np.multiply( 1.0 / a, L)
	minusI = np.diag( [-1.0 for x in range(len(L))])
	Translated_L = np.add( aL, minusI )
    # f, T_0(Translated_L)f, ...
	functions = compute_polynomials_matrices_againstFunction(Translated_L, f, N) 
	# write psi(sL)f as a linear combination of T_k(L)f
	approximation = np.multiply( - 0.5 * coeffs[0], f )
	for k in range(N):
		c_k = coeffs[k]  
		T_k = functions[k]
		approximation = np.add(approximation, np.multiply( c_k, T_k ))
	return approximation

def compute_PIf_withIndicator(f, L, lambda_1, I_min, I_max, N=30, printLogs=False):
	# compute P_I(L)f, where L is Laplacian
	# use Chebyshev approximation on indicator function P_I
	n = len(L)
	projection = lambda x: P_I(x, I_min, I_max)
    # Chebyshev approx. of projection(L)f
	approximation = SGWT_coeff(projection, L, f, 1, N, lambda_1, n)  
	if printLogs:
		print("\nI = [%f, %f]" % (I_min, I_max))
		print("\nf = %s" % str(f))
		print("\nf_I = %s" % str(approximation))
	return approximation

def norm2(f1, f2):
	norm = [ m.pow(x-y, 2) for x, y in zip(f1, f2)]
	norm = m.sqrt( sum(norm) )
	return norm

def count_eigenvalues(L, lambda_1, n_v, Imin, Imax, evalues=[]):
    # estimate the count of eignevalues of L in interval [Imin, Imax
    # n_v the number of random vectors generate
    N = 30
    n = len(L)
    mu_I = 0.0
    np.random.seed(seed=0)
    for k in range(n_v):
        v_k = np.random.normal(0, 1.0, n)
        v_k = v_k / norm2(v_k, [0.0]*n)
        P_I_v_k = compute_PIf_withIndicator(v_k, L, lambda_1, Imin, Imax, N=N, 
                                            printLogs=False)
        mu_I += np.dot(v_k, P_I_v_k)
        if k % 100 == 0 and not k == 0:
            tmp = (mu_I * n) / float(k)
            print("step %d : temp count = %f" % (k, round(tmp)))
    mu_I = (mu_I * n) / float(n_v)
    if len(evalues) > 0:
        selected_evalues = [ l for l in evalues if l<=Imax]
        selected_evalues = [ l for l in selected_evalues if l >=Imin]
        true_count = len(selected_evalues)
        print("true eigenvalues in interval I=[%f, %f]\n%s" % (Imin, Imax, str(selected_evalues)))
        print("count = %f" % mu_I)
        print("true count = %f" % true_count)
    return round(mu_I)

def get_intervalsAndEigenvalueCounts(L, lambda_1, window_size, jump_length):
	intervals = []
	counts = []
	Imin, Imax = 0.0, window_size
	while lambda_1 + window_size > Imax:
		I = [Imin, Imax]
		count_I = count_eigenvalues(L, lambda_1, 50, Imin, Imax)
		intervals.append(I)
		counts.append(count_I)
		Imin += jump_length
		Imax += jump_length

	return intervals, counts

def get_eigenvalueCounts(evalues, intervals):
	counts = []
	print(evalues)
	for I in intervals:
		intersection = [ e for e in evalues if e >= I[0]]
		intersection = [ e for e in intersection if e <= I[1]]
		count = len(intersection)
		counts.append(count)
	return counts

def noise_signal(f, sigma):
	n = len(f)
	z = np.random.normal(0,sigma,n)
	return f+z

def snr(x, y):
    z = 20 * np.log10(np.linalg.norm(x) / np.linalg.norm(x - y))
    return z

def gen_thresholds(min_threshold, max_threshold, nb_thresholds):
	nb_thresholds = float(nb_thresholds)
	pas = (max_threshold - min_threshold) / nb_thresholds
	thresholds = [ float(min_threshold + x * pas ) for x in range(0, int(nb_thresholds))]
	return thresholds

def get_fixed_Laplacian_and_signal(pathToLocal):
	with open(pathToLocal, 'rb') as file:
		L,f = pickle.load( file )
	return L, f

def frame(evalues,evectors,b=2):
    lmax=max(evalues)
    kmax=int(np.log(lmax)/np.log(b))+2
    N=len(evalues)
    r = []
    for k in range(kmax+1):
        print("step %d/%d in frame" % (k, kmax))
        for l in range(N):
            tmp = 0
            for i in range(N):
                tmp += np.sqrt(zeta(evalues[i],k,b))*evectors[l,i]*evectors[:,i]
            r.append(tmp)
    return r

def get_moreOrLessSmoothSignals(pathToLocal):
	with open(pathToLocal, 'rb') as file:
		res_dict = pickle.load( file )
	return res_dict

def load_localized_signals2(pathToLocal):
	with open(pathToLocal, 'rb') as file:
		signals = pickle.load( file )
	return signals

def compute_discreteTransform(phi, psi, L, f, scales, N, lambda_1, n):
	# transform is given by Wf = ( phi(L)f ^T, psi(s_1 L)f ^T, ... ) ^T
	# here we only compute ( phi(L), psi(s_1)L, ...)
	c_0 = SGWT_coeff(phi, L, f, 1.0, N, lambda_1, n)
	c = [c_0]
	for s in scales:
		c_k = SGWT_coeff(psi, L, f, s, N, lambda_1, n)
		c.append(c_k)
	return c

def compute_inverseTransform(phi, psi, G, L, wavelet_coeffs, scales, N, lambda_1, n):
	# use formula (WW^*)^-1.W^*(eta_0,eta_1,...)=(phi/G)(L)eta_0+sum_j(psi(s_j.)/G)(L)eta_j
	kernel_0 = lambda x: phi(x) / G(x)
	eta_0 = wavelet_coeffs[0]
	res = SGWT_coeff(kernel_0, L, eta_0, 1.0, N, lambda_1, n)
	for k, s in enumerate(scales):
		kernel_j = lambda x: psi(s * x) / G(x)
		eta_j = wavelet_coeffs[k+1]
		term_j = SGWT_coeff(kernel_j, L, eta_j, 1.0, N, lambda_1, n)
		res = np.add(res, term_j)
	return res

def soft_threshold(line, threshold):
	Line = []
	for k, x in enumerate(line):
		if x < - threshold:
			Line.append(x + threshold)
		elif x > threshold:
			Line.append(x - threshold)
		else:
			Line.append( 0.0 )
	return Line

def softThreshold(z,c):
    if z>=0:
        return max(z-c,0)
    else:
        return -max(-z-c,0)

def analysis(f,tf):
    coeff = []
    for e in tf:
        coeff.append(np.inner(f,e))
    return coeff
        
def synthesis(coeff,tf):
    f=0
    for i in range(len(coeff)):
        f+=coeff[i]*tf[i]
    return f

def fhat(y,tf,sigma=1,t=1):
    noisyCoeffs = analysis(y,tf)
    thresholdedCoeffs = []
    for i in range(len(noisyCoeffs)):
        c = sigma*np.linalg.norm(tf[i])*t
        thresholdedCoeffs.append(softThreshold(noisyCoeffs[i],c))
    return synthesis(thresholdedCoeffs,tf)

def getFromPickle(pathToPickle):
    with open(pathToPickle,'rb') as file:
        L,evalues,evectors = pickle.load(file)
    return L,evalues,evectors

def blanchard_toPickle():
	pathToPickle="swissroll_pickle"
	L,evalues,evectors = getFromPickle(pathToPickle)
	tf = frame(evalues,evectors)
	pathToPickle_output = "tf_pickle2"
	with open(pathToPickle_output, 'wb') as file:
		pickle.dump(tf, file)
        
def load_frameBlanchard():
	pathToPickle_output = "tf_pickle2"
	with open(pathToPickle_output, 'rb') as file:
		tf = pickle.load(file)
	return tf

def denoise_coeffs(wavelets_coefficients, threshold):
	# smoothness argument used if we do not want to denoise some scales
	# param nb_safeScales measures the number of scales to be untouched
	new_coeffs = []
	assert type(threshold) in [type(0.0), type([])], "threshold arg type should be float or list"
	if type(threshold)==type(0.0):
			for col in wavelets_coefficients:
				thresholded_coeffs = soft_threshold(col, threshold)
				new_coeffs.append(thresholded_coeffs)
	else:
		for j, col in enumerate(wavelets_coefficients):
			threshold_j = threshold[j]
			thresholded_coeffs = soft_threshold(col, threshold_j)
			new_coeffs.append(thresholded_coeffs)
	return new_coeffs

def search_pureNoise(f, L, lambda_1, sigma, intervals, alpha, counts_I, N=30):
	# search intervals with pure noise in Fourier support
    n = len(f)
    f_projected = [0.0] * n
    support_intervals = []
    for k, I in enumerate(intervals):
        I_min, I_max = I[0], I[1]
        f_I = compute_PIf_withIndicator(f, L, lambda_1, I_min, I_max, N=N, printLogs=False)
        count_I = counts_I[k]
        if count_I > 0:
            # check if f_I is pure noise
            pValue = 1-chi2.cdf( m.pow(norm2(f_I, [0.0]*n), 2) / m.pow(sigma,2), df=count_I )
            print(pValue)
            if pValue < alpha:
                # mean is non-zero => it is not pure noise
                if len(support_intervals) > 0:
                    last_Imax = support_intervals[-1][1]
                    if I_min < last_Imax:
                        support_intervals[-1][1] = I_max
                    else:
                        support_intervals.append( [I_min, I_max] )
                else:
                    support_intervals.append( [I_min, I_max] )
    print("intervals found :\n%s\n" % str(support_intervals))
    for I in support_intervals:
        I_min, I_max = I[0], I[1]
        f_I = compute_PIf_withIndicator(f, L, lambda_1, I_min, I_max, N=N, printLogs=False)
        f_projected = np.add(f_I, f_projected)
    return f_projected

def swissroll(N=500, seed=1, a=1, b=4):
    np.random.seed(seed)
    x =np.random.random_sample(N)
    y=np.random.random_sample(N)
    Sx=np.pi*np.sqrt((b**2-a**2)*x+a**2)
    px=Sx*np.cos(Sx)
    py=np.pi**2*(b**2-a**2)*y/2
    pz=Sx*np.sin(Sx)
    return np.array([px,py,pz])

def gaussian(points,f,N=500,s=0):
    res=np.zeros((N,N))
    for i in range(N):
        for j in range(N):
            val = f(np.linalg.norm(points[:,i]-points[:,j]))
            if np.abs(val) > s:
                res[i,j]=val
            else:
                res[i,j]=0
    return res

def laplacian(W):
    D=np.diag(np.sum(W,axis=1))
    return D-W

def generate_Laplacian(p, size, max=100, printLogs=False):
	# generate a (non-normalized) Laplacian matrix with sparsity p, and nrow=ncol=size
    T = []
    diag_coeffs = []
    for k in range(size):
        line = np.random.binomial(1, p, size)
        for l in range(size):
            if l >= k:
                line[l] = 0
        coeffs = np.random.randint(max, size=size)
        for k, coeff in enumerate(coeffs):
        	if coeff != 0:
        		coeffs[k] = - float(coeff)
        line = line * coeffs
        T.append(line)
    S = np.add( T, np.transpose(T))
    for line in S:
        diag_coeffs.append(- sum(line))
    D = np.diag(diag_coeffs)
    S = np.add(D, S)
    if printLogs:
    	print("triangular: \n %s" % str(T))
    	print("diagonal: \n%s" % str(D))
    	print("Laplacian: \n%s" % str(S))
    return S

def load_configuration_experiment(n, eigenvalue_rank, window_size, laplacian_pickle_name, signals_pickle_name):
    print("\n ======= loading experimentation parameters ======= \n")
    if n == 1000:
        assert eigenvalue_rank in [0, 50, 94], "eigenvalue_rank should be in [0, 50, 94]"
        sigma = 0.005
        alpha = 0.001
        jump_length = window_size
        L,evalues,evectors = getFromPickle(laplacian_pickle_name)
        lambda_1 = evalues[-1]
        lambda_1 = evalues[-1]
		# compute true projection interval for oracle case
        if eigenvalue_rank == 94:
            Imin, Imax = 7.17, 8.78
            print("\neigenvalues in support [%f, %f]:\n%s\n" % ( Imin, Imax, str(evalues[940:990])))
        if eigenvalue_rank == 50:
            Imin, Imax = 4.52, 4.76
            print("\neigenvalues in support [%f, %f]:\n%s\n" % (Imin, Imax, str(evalues[500:550])))
        if eigenvalue_rank == 0:
            Imin, Imax = 0.0, 0.61
            print("\neigenvalues in support [%f, %f]:\n%s\n" % (Imin, Imax, str(evalues[0:50])))
        tf = load_frameBlanchard()
        signals = load_localized_signals2(signals_pickle_name)
        f = signals[eigenvalue_rank]
        return L, lambda_1, evalues, evectors, f, tf, sigma, alpha, window_size, jump_length, Imin, Imax
    elif n == 10:
        assert eigenvalue_rank in list(range(1, 11)) + [12, 56, 910], "eigenvalue_rank should be in [1, ..., 10, 12, 56, 910]"
        print("oracle projection not available for n=%d" % n)
        L, _ = get_fixed_Laplacian_and_signal(laplacian_pickle_name)
        evalues,evectors = np.linalg.eigh(L)
        lambda_1 = evalues[-1]
        print("eigenvalues of L:\n%s" % str(evalues))
        tf = frame(evalues,evectors)
        sigma = 0.05
        alpha = 0.001
        jump_length = window_size
        signals = get_moreOrLessSmoothSignals(signals_pickle_name)
        if not eigenvalue_rank in [12, 56, 910]:
            f = signals[eigenvalue_rank]
            print("signal f:\n%s" % str(f))
            Imin = max(evalues[10 - eigenvalue_rank] - 0.5, 0.0)
            Imax = min(evalues[10 - eigenvalue_rank] + 0.5, lambda_1)
        else:
            if eigenvalue_rank == 12:
                f = [0.0] * n
                for k in range(1,3):
                    f = np.add( f, signals[k] )
                Imin = max(evalues[8] - 0.5, 0.0)
                Imax = lambda_1 + 5.0
            if eigenvalue_rank == 56:
                f = [0.0] * n
                for k in range(5,7):
                    f = np.add( f, signals[k] )
                Imin = max(evalues[4] - 0.5, 0.0)
                Imax = evalues[5] + 0.5
            if eigenvalue_rank == 910:
                f = [0.0] * n
                for k in range(9,11):
                    f = np.add( f, signals[k] )
                Imin = max(evalues[0] - 0.5, 0.0)
                Imax = evalues[1] + 0.5
            f = f/norm2(f, [0.0]*n)
        return L, lambda_1, evalues, evectors, f, tf, sigma, alpha, window_size, jump_length, Imin, Imax

def comparison_blanchardVSall(nb_noise_iterations, n, eigenvalue_rank, window_size, laplacian_pickle_name, signals_pickle_name, phi, psi, scales, thresholds, thresholds2):
	# comparison between Blanchard VS no denoising VS Gribonval denoising VS Loclet
	# Loclet depends on projection_mode param: projection_mode in [ "oracle_projection", "chi2_projection", "old_searchSupport_projection" ]
	N = 30
	N2 = 100
	G = lambda x: Gf(x, phi, psi, scales)
	print("\n ======= denoising, comparison to Blanchard ======= \n")
	print("graph size = %d, and signal with id = %d" % (n, eigenvalue_rank))
	print("window size = %f" % window_size)
	print("wavelet scales:\n%s" % str(scales))
	print("\n ======= ================================== ======= \n")
	L, lambda_1, evalues, evectors, f, tf, sigma, alpha, window_size, jump_length, Imin, Imax = load_configuration_experiment(n, eigenvalue_rank, window_size, laplacian_pickle_name, signals_pickle_name)
	print("I = [%f, %f]" % (Imin, Imax))
	evalues = [abs(e) for e in evalues]  # to avoid numerical flaws with evalue < 0
	print("\n ======= estimating intervals and count with chi2 ======= \n")
	intervals, counts = get_intervalsAndEigenvalueCounts(L, lambda_1, window_size, jump_length)
	if n==10:
		counts = get_eigenvalueCounts(evalues, intervals)
		print(counts)
	results_errors = []
	results_thresholds = []
	best_functions = {'f':[], 'f_noisy':[], 'best_PF':[], 'best_LLet':[], 'best_LLet+PF':[]}
	best_functions['f'] = f
	for noise_id in range(0, nb_noise_iterations):
		f_noisy = noise_signal(f, sigma)
		best_functions['f_noisy'] = f_noisy
		error_baseline = snr(f, f_noisy)
		# initialization of min errors, thresholds loggers
		min_error, min_error_2, min_error_blanchard, min_threshold_blanchard = 0.0, 0.0, 0.0, 0.0
		min_thresholds, min_thresholds_2 = [0.0, 0.0], [0.0, 0.0]
		nb_steps = 0
		total_nb_steps = len(thresholds) * len(thresholds2)
		# find Fourier support
		f_noisy_I2 = search_pureNoise(f_noisy, L, lambda_1, sigma, intervals, alpha, counts, N=N2)
		f_noisy_I1 = np.subtract(f_noisy, f_noisy_I2)
		print("I = [%f, %f]" % (Imin, Imax))
		print("f = %s ..." % str(f[:5]))
		print("f_noisy = %s ..." % str(f_noisy[:5]))
		print("f noisy projection outside Fourier support = %s ..." % str(f_noisy_I1[:5]))
		print("f noisy projection on Fourier support = %s ..." % str(f_noisy_I2[:5]))
		noisy_wavelet_coeffs = compute_discreteTransform(phi, psi, L, f_noisy_I1, scales, N, lambda_1, n)  # coeffs corresponding to projection outside support
		noisy_wavelet_coeffs1 = compute_discreteTransform(phi, psi, L, f_noisy_I2, scales, N, lambda_1, n)  # coeffs corresponding to projection on support
		print("\n ======= thresholding ======= \n")
		for threshold in thresholds:
			thresholded_coeffs = denoise_coeffs(noisy_wavelet_coeffs, threshold)
			f_noisy_I1_N_hat = compute_inverseTransform(phi, psi, G, L, thresholded_coeffs, scales, N, lambda_1, n)
			error_twoThreshold, error_twoThreshold_2 = 0.0, 0.0
			min_threshold2, min_threshold_2 = 0.0, 0.0
			best_LLet, best_LLetPF = [], []
			for threshold_2 in thresholds2:
				nb_steps += 1
				if nb_steps % 10 == 0:
					print("noise_id=%d , nb_steps=%d/%d" % (noise_id, nb_steps, total_nb_steps))
				denoised = fhat(f_noisy,tf,sigma=sigma,t=threshold_2)
				error_blanchard = snr(f, denoised)
				#print("threshold = %f , error_blanchard = %f" % (threshold_2, error_blanchard))
				thresholded_coeffs1 = denoise_coeffs(noisy_wavelet_coeffs1, threshold_2)
				f_noisy_I2_N_hat = compute_inverseTransform(phi, psi, G, L, thresholded_coeffs1, scales, N, lambda_1, n)
				denoised2 = fhat(f_noisy_I2,tf,sigma=sigma,t=threshold_2)
				# estimators
				f_hat = f_noisy_I1_N_hat + f_noisy_I2_N_hat
				f_hat_2 = f_noisy_I1_N_hat + denoised2
				# errors of estimators
				error_hat_1 = snr(f, f_hat)
				error_hat_2 = snr(f, f_hat_2)
				if error_hat_1 > error_twoThreshold :
					error_twoThreshold = error_hat_1
					min_threshold2 = threshold_2
					best_LLet = f_hat
				if error_hat_2 > error_twoThreshold_2 :
					error_twoThreshold_2 = error_hat_2
					min_threshold_2 = threshold_2
					best_LLetPF = f_hat_2
				if error_blanchard > min_error_blanchard:
					min_error_blanchard = error_blanchard
					min_threshold_blanchard = threshold_2
					best_functions['best_PF'] = denoised
			if error_twoThreshold > min_error:
				min_error = error_twoThreshold
				min_thresholds = [threshold, min_threshold2]
				best_functions['best_LLet'] = best_LLet
			if error_twoThreshold_2 > min_error_2:
				min_error_2 = error_twoThreshold_2
				min_thresholds_2 = [threshold, min_threshold_2]
				best_functions['best_LLet+PF'] = best_LLetPF
		# collect best snr errors
		line = [eigenvalue_rank, noise_id, window_size, error_baseline, min_error_blanchard, min_error, min_error_2]
		results_errors.append( line )
		# collect optimal thresholds
		line2 = [eigenvalue_rank, noise_id, window_size, min_threshold_blanchard, min_thresholds[1], min_thresholds_2[1]]
		results_thresholds.append(line2)
		print("\n ======= 	results   ======= \n")
		print("\nw ith no denoising = %f" % (error_baseline))
		print("\nbest with blanchard = %f\nwith threshold = %f" % (min_error_blanchard, min_threshold_blanchard))
		print("\nbest with Loclet = %f\nwith thresholds = %s" % (min_error, str(min_thresholds)))
		print("\nbest with Loclet + blanchard on Fourier support = %f\nwith thresholds = %s\n" % (min_error_2, str(min_thresholds_2)))

if __name__=='__main__':
    phi, psi = lambda x: m.sqrt(zeta(x, 0)), lambda x: m.sqrt(zeta(x, 1))
    scales = [ m.pow( 0.5, k ) for k in range(6)]
    MC = 4 # number of iteration
    # eigenvalue_rank = id of signal used in experiment
    # n = 1000, eigenvalue_rank in [0, 50, 94]
    # n = 10, eigenvalue_rank in [1,2, ..., 10, 12, 56, 910]
    n=1000
    e_rank=0
    #n = 10
    #e_rank = 1
    if n == 1000:
        #uncomment to generate frame pickle
        #blanchard_toPickle()
        window_size = 1.0
        L_pickle = 'swissroll_pickle'
        f_pickle = './localized_signals3_n=1000'
        thresh = gen_thresholds(0.1, 5.0, 1)  
        thresh2 = gen_thresholds(0.0001, 5.0, 50)
    elif n == 10:
        window_size = 10.0
        L_pickle = 'laplacian_signal_3'
        f_pickle = './localized_signals'
        thresh = gen_thresholds(0.01, 0.055, 10)
        thresh2 = gen_thresholds(0.001, 3.0, 30)
    comparison_blanchardVSall(MC, n, e_rank, window_size, L_pickle, f_pickle, 
                              phi, psi, scales, thresh, thresh2)
